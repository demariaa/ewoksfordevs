import os
import json
from contextlib import ExitStack

from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
from pyFAI.detectors import detector_factory

from blissdata.h5api import dynamic_hdf5
from silx.io import h5py_utils
from ewoksdata.data.hdf5.dataset_writer import DatasetWriter
from blissoda.utils.directories import get_processed_dir

from ewokscore import Task


class PyFaiIntegrate(
    Task,
    input_names=[
        "filename",
        "scan",
        "lima_name",
        "detector",
        "calibration",
    ],
    optional_input_names=["detector_config"],
    output_names=["uri"],
):
    def run(self):
        # Prepare saving
        processed_dir = get_processed_dir(self.inputs.filename)
        os.makedirs(processed_dir, exist_ok=True)
        outfilename = os.path.join(
            processed_dir, os.path.basename(self.inputs.filename)
        )

        # Perpare integration
        detector = detector_factory(
            self.inputs.detector,
            config=self.get_input_value("detector_config", dict()),
        )
        ai = AzimuthalIntegrator(
            **self.inputs.calibration, detector=detector
        )

        with ExitStack() as stack:
            # Wait for the scan to start
            ctx = dynamic_hdf5.File(
                self.inputs.filename,
                lima_names=[self.inputs.lima_name],
            )
            fin = stack.enter_context(ctx)
            images = fin[
                f"/{self.inputs.scan}/instrument/{self.inputs.lima_name}/data"
            ]

            # Initialize saving
            ctx = h5py_utils.open_item(outfilename, "/", mode="a")
            nxroot = stack.enter_context(ctx)
            if self.inputs.scan in nxroot:
                self.outputs.uri = (
                    f"{nxroot.file.filename}::/{self.inputs.scan}"
                )
                print(f"Already processed : {self.outputs.uri}")
                return

            # Prepare NeXus structure for saving
            nxroot.attrs["NX_class"] = "NXroot"

            nxentry = nxroot.create_group(self.inputs.scan)
            nxroot.attrs["default"] = self.inputs.scan
            nxentry.attrs["NX_class"] = "NXentry"

            nxprocess = nxentry.create_group("integrate")
            nxentry.attrs["default"] = "integrate"
            nxprocess.attrs["NX_class"] = "NXprocess"
            nxprocess["program"] = "pyFAI"

            nxdata = nxprocess.create_group("results")
            nxprocess.attrs["default"] = "results"
            nxdata.attrs["NX_class"] = "NXdata"
            nxdata.attrs["interpretation"] = "spectrum"
            nxdata.attrs["signal"] = "intensity"
            nxdata.attrs["axes"] = "scattering_angle"

            nxnote = nxprocess.create_group("config")
            nxnote.attrs["NX_class"] = "NXnote"
            nxnote["data"] = json.dumps(ai.get_config())
            nxnote["type"] = "application/json"

            radial = None

            # Writer for the integrated data
            ctx = stack.enter_context(
                DatasetWriter(nxdata, "intensity")
            )
            intensity_writer = stack.enter_context(ctx)

            # Loop over images, integrate and save
            # During the scan the processing happens in blocks of 100
            # (1 lima file contains 100 images)
            for i, image in enumerate(images):
                result = ai.integrate1d(
                    image[:-1, :-1], npt=4096, unit="2th_deg"
                )
                if radial is None:
                    radial = nxdata.create_dataset(
                        "scattering_angle", data=result.radial
                    )
                    radial.attrs[
                        "long_name"
                    ] = "Scattering angle 2θ (degrees)"
                hasnewdata = intensity_writer.add_point(
                    result.intensity
                )
                if hasnewdata:
                    nxroot.file.flush()
                if ((i + 1) % 10) == 0:
                    print(f"processed {i+1} images")

            self.outputs.uri = (
                f"{nxroot.file.filename}::/{self.inputs.scan}"
            )
