from ewokscore import Task  # noqa E402


class Add(
    Task,
    input_names=["a"],
    optional_input_names=["b"],
    output_names=["sum"],
):
    def run(self):
        if self.missing_inputs.b:
            self.outputs.sum = self.inputs.a
        else:
            self.outputs.sum = self.inputs.a + self.inputs.b


import numpy  # noqa E402


class Random1(
    Task,
    optional_input_names=["low", "high", "size"],
    output_names=["numbers"],
):
    def run(self):
        low = self.get_input_value("low", 0)
        high = self.get_input_value("high", 1)
        size = self.get_input_value("size", 1)
        self.outputs.numbers = numpy.random.uniform(
            low=low, high=high, size=size
        )


class Random2(
    Task,
    optional_input_names=["low", "high", "size"],
    output_names=["numbers"],
):
    def run(self):
        self.outputs.numbers = numpy.random.uniform(
            **self.input_values
        )
