Ewoks for developers
====================

Participants:
-------------

Download all tutorial resources

```
git clone https://gitlab.esrf.fr/workflow/ewokstutorials/ewoksfordevs.git
```

The slideshow can be found [here](https://workflow.gitlab-pages.esrf.fr/ewokstutorials/ewoksfordevs/) or
as PDF ([slides](https://workflow.gitlab-pages.esrf.fr/ewokstutorials/ewoksfordevs/ewoksfordevs.slides.pdf),
[book](https://workflow.gitlab-pages.esrf.fr/ewokstutorials/ewoksfordevs/ewoksfordevs.pdf)).

Contibute to this tutorial:
---------------------------

```
pip install -r requirement.txt
```

See the slideshow

```
jupyter nbconvert ewoksfordevs.ipynb --embed-images --to slides --post serve
```

Create static slideshow

```
jupyter nbconvert ewoksfordevs.ipynb --embed-images --to slides
jupyter nbconvert ewoksfordevs.ipynb --embed-images --to pdf
```

Generate all notebook results

```
jupyter nbconvert ewoksfordevs.ipynb --to notebook --execute
```

Linting notebooks

```
black .
```
