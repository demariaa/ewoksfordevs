#!/usr/bin/env bash
jupyter nbconvert ewoksfordevs.ipynb --to notebook --execute
jupyter nbconvert ewoksfordevs.nbconvert.ipynb --to slides --embed-images

if ! command -v decktape &> /dev/null
then
    # npm install decktape
    export decktape_cmd=$(npm bin)/decktape
else
    # npm install decktape -g
    export decktape_cmd="decktape"
fi

$decktape_cmd generic --key=Space ewoksfordevs.nbconvert.slides.html ewoksfordevs.slides.pdf
